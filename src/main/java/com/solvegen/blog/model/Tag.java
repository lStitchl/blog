package com.solvegen.blog.model;

import org.hibernate.validator.constraints.NotBlank;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Tag {

    private Long id;

    @NotBlank
    private String content;

    public Tag() {

    }

    public Tag(@NotBlank String content) {
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
