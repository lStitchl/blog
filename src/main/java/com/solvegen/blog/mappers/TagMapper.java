package com.solvegen.blog.mappers;


import com.solvegen.blog.model.Article;
import com.solvegen.blog.model.Tag;
import org.apache.ibatis.annotations.*;

public interface TagMapper {

    Long save(Tag tag);

    Tag find(Long id);

    void delete(Tag tag);

    void update(Tag tag);

    void saveArticleDep(@Param("article") Article article, @Param("tag") Tag tag);

}
