package com.solvegen.blog.mappers.oracleMappers;

import com.solvegen.blog.mappers.ArticleMapper;
import com.solvegen.blog.model.Article;
import com.solvegen.blog.model.Comment;
import com.solvegen.blog.model.Tag;
import com.solvegen.blog.model.User;
import org.apache.ibatis.annotations.*;

import java.util.Collection;

public interface ArticleOracleMapper extends ArticleMapper{
    @Insert("INSERT INTO article(user_id, article_title, "
            + "article_content, article_created_at, "
            + "article_modified_at, user_id_modified_by) "
            + "VALUES (#{author.id}, #{title}, #{content}, "
            + "#{createdAt}, #{modifiedAt}, #{author.id})")
    @SelectKey(
            keyProperty = "id",
            before = false,
            resultType = Long.class,
            statement = { "SELECT ARTICLE_SEQ.currval AS id FROM dual" })
//    @Options(useGeneratedKeys = true, keyProperty = "id", flushCache = true, keyColumn = "id")
    Long save(Article article);

    @Update("UPDATE article SET user_id = #{author.id}, "
            + "article_title = #{title}, article_content = #{content}, "
            + "article_created_at = #{createdAt}, "
            + "article_modified_at = #{modifiedAt}, "
            + "user_id_modified_by = #{author.id}")
    void update(Article article);

    @Delete("DELETE FROM article WHERE article_id = #{article.id}")
    void delete(@Param("article") Article article);

    @Select("SELECT A.article_id as ID, A.user_id as AUTHOR, "
            + "A.article_title as TITLE, A.article_content as CONTENT, "
            + "A.article_created_at as CREATEDAT, "
            + "A.article_modified_at as MODIFIEDAT, "
            + "A.user_id_modified_by as MODIFIEDBY "
            + "FROM article A  "
            + "WHERE A.article_id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "author", column = "AUTHOR", javaType = User.class,
                    one = @One(select = "com.solvegen.blog.mappers.oracleMappers.UserOracleMapper.find")),
            @Result(property = "title", column = "TITLE"),
            @Result(property = "createdAt", column = "CREATEDAT"),
            @Result(property = "modifiedAt", column = "MODIFIEDAT"),
            @Result(property = "modifiedBy", column = "MODIFIEDBY", javaType = User.class,
                    one = @One(select = "com.solvegen.blog.mappers.oracleMappers.UserOracleMapper.find")),
            @Result(property = "comments", column = "ID", javaType = Collection.class, many =
            @Many(select = "com.solvegen.blog.mappers.oracleMappers.ArticleOracleMapper.findCommentsByArticleId")),
            @Result(property = "tags", column = "ID", javaType = Collection.class, many =
            @Many(select = "com.solvegen.blog.mappers.oracleMappers.ArticleOracleMapper.findTagsByArticleId"))
    })
    Article find(Long id);

    @Select("SELECT A.article_id as ID, A.user_id as AUTHOR, "
            + "A.article_title as TITLE, A.article_content as CONTENT, "
            + "A.article_created_at as CREATEDAT, "
            + "A.article_modified_at as MODIFIEDAT, "
            + "A.user_id_modified_by as MODIFIEDBY "
            + "FROM article A ")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "author", column = "AUTHOR", javaType = User.class,
                    one = @One(select = "com.solvegen.blog.mappers.oracleMappers.UserOracleMapper.find")),
            @Result(property = "modifiedBy", column = "MODIFIEDBY", javaType = User.class,
                    one = @One(select = "com.solvegen.blog.mappers.oracleMappers.UserOracleMapper.find")),
            @Result(property = "comments", column = "ID", javaType = Collection.class, many =
            @Many(select = "com.solvegen.blog.mappers.oracleMappers.ArticleOracleMapper.findCommentsByArticleId")),
            @Result(property = "tags", column = "ID", javaType = Collection.class, many =
            @Many(select = "com.solvegen.blog.mappers.oracleMappers.ArticleOracleMapper.findTagsByArticleId"))
    })
    Collection<Article> findAll();

    @Select("SELECT comment_id as ID, "
            + "user_id as AUTHOR, "
            + "content as CONTENT, "
            + "comment_created_at as CREATEDAT, "
            + "comment_modified_at as MODIFIEDAT, "
            + "user_id_modified_by as MODIFIEDBY, "
            + "article_id as ARTICLE "
            + "FROM \"COMMENT\" WHERE article_id = #{id} ")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "author", column = "AUTHOR", javaType = User.class,
                    one = @One(select = "com.solvegen.blog.mappers.oracleMappers.UserOracleMapper.find")),
            @Result(property = "modifiedBy", column = "MODIFIEDBY", javaType = User.class,
                    one = @One(select = "com.solvegen.blog.mappers.oracleMappers.UserOracleMapper.find"))
    })
    Collection<Comment> findCommentsByArticleId(Long id);

    @Select("SELECT tag.tag_id as id, tag.tag_content as content " +
            "FROM tag_article " +
            "INNER JOIN tag ON tag_article.tag_id = tag.tag_id " +
            "WHERE tag_article.article_id = #{id}")
    Collection<Tag> findTagsByArticleId(Long id);

    @Delete("DELETE FROM tag_article " +
            "WHERE article_id = #{article.id} AND tag_id = #{tag.id}")
    void deleteTagDep(@Param("article") Article article, @Param("tag") Tag tag);

}