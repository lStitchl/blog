package com.solvegen.blog.mappers.oracleMappers;

import com.solvegen.blog.mappers.UserMapper;
import com.solvegen.blog.model.User;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

public interface UserOracleMapper extends UserMapper{
    @Insert("INSERT INTO \"USER\"(user_name, user_sirname, "
            + "user_nick, user_password, user_birthday, "
            + "user_email, user_tel, user_info) VALUES "
            + "(#{name}, #{sirname}, #{nickname}, #{password}, "
            + "#{dateOfBirth}, #{email}, #{tel}, #{info})")
    @Arg(jdbcType = JdbcType.NULL)
    @SelectKey(
            keyProperty = "id",
            before = false,
            resultType = Long.class,
            statement = { "SELECT USER_SEQ.currval AS id FROM dual" })
//    @Options(useGeneratedKeys = true, keyProperty = "id", flushCache = true, keyColumn = "user_id")

    Long save(User user);

    @Select("SELECT user_id as ID, user_name as NAME, "
            + "user_sirname as SIRNAME,"
            + "user_nick as NICKNAME,user_password as PASSWORD, "
            + "user_birthday as DATEOFBIRTH, "
            + "user_email as EMAIL, user_tel as TEL, "
            + "user_info as INFO "
            + "FROM \"USER\" WHERE user_id = #{id}")
    User find(Long id);

    @Delete("DELETE FROM \"USER\" WHERE user_id = #{id}")
    void delete(User user);

    @Update("UPDATE \"USER\" SET user_name = #{name}, user_sirname = #{sirname}, "
            + "user_nick = #{nickname}, user_password = #{password}, "
            + "user_birthday = #{dateOfBirth}, user_email = #{email}, "
            + "user_tel = #{tel}, user_info = #{info} WHERE user_id = #{id}")
    //@Options(useGeneratedKeys = true, keyProperty = "id", flushCache = true, keyColumn = "user_id")
    Long update(User user);

}
