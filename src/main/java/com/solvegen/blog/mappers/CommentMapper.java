package com.solvegen.blog.mappers;

import com.solvegen.blog.model.Article;
import com.solvegen.blog.model.Comment;
import com.solvegen.blog.model.User;
import org.apache.ibatis.annotations.*;

public interface CommentMapper {

    Long save(Comment comment);

    void update(Comment comment);

    Comment find(Long id);

    void delete(Comment comment);

}
