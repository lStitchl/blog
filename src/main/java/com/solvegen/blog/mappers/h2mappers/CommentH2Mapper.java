package com.solvegen.blog.mappers.h2mappers;

import com.solvegen.blog.mappers.CommentMapper;
import com.solvegen.blog.model.Article;
import com.solvegen.blog.model.Comment;
import com.solvegen.blog.model.User;
import org.apache.ibatis.annotations.*;

public interface CommentH2Mapper extends CommentMapper {

    @Insert("INSERT INTO comment(user_id, comment_created_at, "
            + "comment_modified_at, user_id_modified_by, "
            + "article_id, content) VALUES "
            + "(#{author.id}, #{createdAt}, #{modifiedAt}, "
            + "#{modifiedBy.id}, #{article.id}, #{content})")
    @Options(useGeneratedKeys = true, keyProperty = "id", flushCache = true, keyColumn = "comment_id")
    Long save(Comment comment);

    @Update("UPDATE comment SET user_id = #{author.id},"
            + "comment_created_at = #{createdAt}, "
            + "comment_modified_at = #{modifiedAt}, "
            + "user_id_modified_by = #{modifiedBy.id}, "
            + "content = #{content} ")
    @Options(useGeneratedKeys = true, keyProperty = "id", flushCache = true, keyColumn = "comment_id")
    void update(Comment comment);

    @Select("SELECT comment_id as ID, "
            + "user_id as AUTHOR, "
            + "content as CONTENT, "
            + "comment_created_at as CREATEDAT, "
            + "comment_modified_at as MODIFIEDAT, "
            + "user_id_modified_by as MODIFIEDBY, "
            + "article_id as ARTICLE "
            + "FROM comment WHERE comment_id = #{id} ")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "author", column = "AUTHOR", javaType = User.class,
                    one = @One(select = "com.solvegen.blog.mappers.h2mappers.UserH2Mapper.find")),
            @Result(property = "article", column = "ARTICLE", javaType = Article.class,
                    one = @One(select = "com.solvegen.blog.mappers.h2mappers.ArticleH2Mapper.find")),
            @Result(property = "modifiedBy", column = "MODIFIEDBY", javaType = User.class,
                    one = @One(select = "com.solvegen.blog.mappers.h2mappers.UserH2Mapper.find"))
    })
    Comment find(Long id);

    @Delete("DELETE FROM comment WHERE comment_id = #{id}")
    void delete(Comment comment);

}
