package com.solvegen.blog.mappers.h2mappers;

import com.solvegen.blog.mappers.ArticleMapper;
import com.solvegen.blog.model.Article;
import com.solvegen.blog.model.Comment;
import com.solvegen.blog.model.Tag;
import com.solvegen.blog.model.User;
import org.apache.ibatis.annotations.*;

import java.util.Collection;

public interface ArticleH2Mapper extends ArticleMapper {

    @Insert("INSERT INTO article(user_id, article_title, "
            + "article_content, article_created_at, "
            + "article_modified_at, user_id_modified_by) "
            + "VALUES (#{author.id}, #{title}, #{content}, "
            + "#{createdAt}, #{modifiedAt}, #{author.id})")
    @Options(useGeneratedKeys = true, keyProperty = "id", flushCache = true, keyColumn = "article_id")

    Long save(Article article);

    @Update("UPDATE article SET user_id = #{author.id}, "
            + "article_title = #{title}, article_content = #{content}, "
            + "article_created_at = #{createdAt}, "
            + "article_modified_at = #{modifiedAt}, "
            + "user_id_modified_by = #{author.id}")
    void update(Article article);

    @Delete("DELETE FROM article WHERE article_id = #{article.id}")
    void delete(@Param("article") Article article);

    @Select("SELECT A.article_id as ID, A.user_id as AUTHOR, "
            + "A.article_title as TITLE, A.article_content as CONTENT, "
            + "A.article_created_at as CREATEDAT, "
            + "A.article_modified_at as MODIFIEDAT, "
            + "A.user_id_modified_by as MODIFIEDBY "
            + "FROM article A  "
            + "WHERE A.article_id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "author", column = "AUTHOR", javaType = User.class,
                    one = @One(select = "com.solvegen.blog.mappers.h2mappers.UserH2Mapper.find")),
            @Result(property = "title", column = "TITLE"),
            @Result(property = "createdAt", column = "CREATEDAT"),
            @Result(property = "modifiedAt", column = "MODIFIEDAT"),
            @Result(property = "modifiedBy", column = "MODIFIEDBY", javaType = User.class,
                    one = @One(select = "com.solvegen.blog.mappers.h2mappers.UserH2Mapper.find")),
            @Result(property = "comments", column = "ID", javaType = Collection.class, many =
            @Many(select = "com.solvegen.blog.mappers.h2mappers.ArticleH2Mapper.findCommentsByArticleId")),
            @Result(property = "tags", column = "ID", javaType = Collection.class, many =
            @Many(select = "com.solvegen.blog.mappers.h2mappers.ArticleH2Mapper.findTagsByArticleId"))
    })
    Article find(Long id);

    @Select("SELECT A.article_id as ID, A.user_id as AUTHOR, "
            + "A.article_title as TITLE, A.article_content as CONTENT, "
            + "A.article_created_at as CREATEDAT, "
            + "A.article_modified_at as MODIFIEDAT, "
            + "A.user_id_modified_by as MODIFIEDBY "
            + "FROM article A ")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "author", column = "AUTHOR", javaType = User.class,
                    one = @One(select = "com.solvegen.blog.mappers.h2mappers.UserH2Mapper.find")),
            @Result(property = "modifiedBy", column = "MODIFIEDBY", javaType = User.class,
                    one = @One(select = "com.solvegen.blog.mappers.h2mappers.UserH2Mapper.find")),
            @Result(property = "comments", column = "ID", javaType = Collection.class, many =
            @Many(select = "com.solvegen.blog.mappers.h2mappers.ArticleH2Mapper.findCommentsByArticleId")),
            @Result(property = "tags", column = "ID", javaType = Collection.class, many =
            @Many(select = "com.solvegen.blog.mappers.h2mappers.ArticleH2Mapper.findTagsByArticleId"))
    })
    Collection<Article> findAll();

    @Select("SELECT comment.comment_id as ID, "
            + "user_id as AUTHOR, "
            + "comment.content as CONTENT, "
            + "comment.comment_created_at as CREATEDAT, "
            + "comment.comment_modified_at as MODIFIEDAT, "
            + "comment.user_id_modified_by as MODIFIEDBY, "
            + "comment.article_id as ARTICLE "
            + "FROM comment WHERE comment.article_id = #{id} ")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "author", column = "AUTHOR", javaType = User.class,
                    one = @One(select = "com.solvegen.blog.mappers.h2mappers.UserH2Mapper.find")),
            @Result(property = "modifiedBy", column = "MODIFIEDBY", javaType = User.class,
                    one = @One(select = "com.solvegen.blog.mappers.h2mappers.UserH2Mapper.find"))
    })
    Collection<Comment> findCommentsByArticleId(Long id);

    @Select("SELECT tag.tag_id as id, tag.tag_content as content " +
            "FROM tag_article " +
            "INNER JOIN tag ON tag_article.tag_id = tag.tag_id " +
            "WHERE tag_article.article_id = #{id}")
    Collection<Tag> findTagsByArticleId(Long id);

    @Delete("DELETE FROM tag_article " +
            "WHERE article_id = #{article.id} AND tag_id = #{tag.id}")
    void deleteTagDep(@Param("article") Article article, @Param("tag") Tag tag);

}
