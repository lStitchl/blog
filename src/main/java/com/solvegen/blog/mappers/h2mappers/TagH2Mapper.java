package com.solvegen.blog.mappers.h2mappers;

import com.solvegen.blog.mappers.TagMapper;
import com.solvegen.blog.model.Article;
import com.solvegen.blog.model.Tag;
import org.apache.ibatis.annotations.*;

public interface TagH2Mapper extends TagMapper{

    @Insert("INSERT INTO tag(tag_content) VALUES "
            + "(#{content}) ")
    @Options(useGeneratedKeys = true, keyProperty = "id", flushCache = true, keyColumn = "tag_id")
    Long save(Tag tag);

    @Select("SELECT tag_id as ID, tag_content as CONTENT "
            + "FROM tag WHERE tag_id = #{id}")
    Tag find(Long id);

    @Delete("DELETE FROM tag WHERE tag_id = #{id}")
    void delete(Tag tag);

    @Update("UPDATE tag SET tag_content = #{content}")
    void update(Tag tag);

    @Insert("INSERT INTO tag_article(tag_id, article_id)" +
            "VALUES(#{tag.id}, #{article.id})")
    void saveArticleDep(@Param("article") Article article, @Param("tag") Tag tag);
}
