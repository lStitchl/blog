package com.solvegen.blog.mappers;

import com.solvegen.blog.model.User;
import org.apache.ibatis.annotations.*;

public interface UserMapper {

    Long save(User user);

    User find(Long id);

    void delete(User user);

    Long update(User user);

}
