package com.solvegen.blog.mappers;

import com.solvegen.blog.model.Article;
import com.solvegen.blog.model.Comment;
import com.solvegen.blog.model.Tag;
import com.solvegen.blog.model.User;
import org.apache.ibatis.annotations.*;

import java.util.Collection;

public interface ArticleMapper {

    Long save(Article article);

    void update(Article article);

    void delete(@Param("article") Article article);

    Article find(Long id);

    Collection<Article> findAll();

    Collection<Comment> findCommentsByArticleId(Long id);

    Collection<Tag> findTagsByArticleId(Long id);

    void deleteTagDep(@Param("article") Article article, @Param("tag") Tag tag);

}
