package com.solvegen.blog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

//@EnableWebMvc
@SpringBootApplication
@ImportResource(value = "classpath:/spring/spring-config.xml")
public class BlogApplication {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(BlogApplication.class, args);
    }
}
