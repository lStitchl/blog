package com.solvegen.blog.services;

import com.solvegen.blog.model.Tag;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Validated
@Service
public interface TagService {

    @NotNull
    Tag newTag();

    Long save(@Valid Tag tag);

    void update(@Valid Tag tag);

    void delete(@NotNull Long id);

    Tag find(@NotNull Long id);

}
