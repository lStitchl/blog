package com.solvegen.blog.services;

import com.solvegen.blog.model.Comment;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Validated
@Service
public interface CommentService {

    @NotNull
    Comment newComment();

    Long save(@Valid Comment comment);

    Long update(@Valid Comment comment);

    void delete(@NotNull (message = "Message id must be set") Long id);

    Comment find(@NotNull Long id);

}
