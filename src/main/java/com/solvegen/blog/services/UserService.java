package com.solvegen.blog.services;

import com.solvegen.blog.model.User;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Validated
@Service
public interface UserService {

    @NotNull
    User newUser();

    Long save(@Valid User user);

    Long update(@Valid User user);

    void delete(@NotNull Long id);

    User find(@NotNull Long id);

}
