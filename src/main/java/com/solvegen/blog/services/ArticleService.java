package com.solvegen.blog.services;

import com.solvegen.blog.exceptions.ArticleNotFoundException;
import com.solvegen.blog.model.Article;
import com.solvegen.blog.model.Comment;
import com.solvegen.blog.model.Tag;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collection;

@Validated
@Service
public interface ArticleService {

    @NotNull
    Article newArticle();

    Long save(@Valid Article article);

    void update(@Valid Article article);

    void delete(@NotNull(message = "ID must be not null") Long id) throws ArticleNotFoundException;

    Article find(@NotNull(message = "ID must be not null") Long id);

    Collection<Article> findAll();

    void addComment(@NotNull Long articleId, Comment comment);

    Article addTags(@NotNull Long articleId, @Valid Tag tag);

    void removeTagsFromArticle(@NotNull Long articleId, @Valid Tag tag);

    Comment getComment(Long articleId, Long commentId);

    Tag getTag(Long articleId, Long tagId);
}
