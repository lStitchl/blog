package com.solvegen.blog.services.mapper;

import com.solvegen.blog.exceptions.ArticleNotFoundException;
import com.solvegen.blog.mappers.ArticleMapper;
import com.solvegen.blog.mappers.CommentMapper;
import com.solvegen.blog.mappers.TagMapper;
import com.solvegen.blog.mappers.h2mappers.ArticleH2Mapper;
import com.solvegen.blog.model.Article;
import com.solvegen.blog.model.Comment;
import com.solvegen.blog.model.Tag;
import com.solvegen.blog.services.ArticleService;
import com.solvegen.blog.services.CommentService;
import com.solvegen.blog.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ArticleServiceMapperImpl implements ArticleService {

    @Autowired(required = false)
    private ArticleMapper articleMapper;

    @Autowired(required = false)
    private CommentMapper commentMapper;

    @Autowired(required = false)
    private TagMapper tagMapper;

    @Autowired(required = false)
    private TagService tagService;

    @Autowired(required = false)
    private CommentService commentService;

    public Article newArticle() {
        return new Article();
    }

    public Long save(Article article) {
        Long savedArticleId = articleMapper.save(article);

        if (article.getTags() != null) {

            for (Tag tag : article.getTags()) {

                if (tagService.find(tag.getId()) == null) {
                    tagMapper.save(tag);
                    tagMapper.saveArticleDep(article, tag);
                }

            }
        }

        return article.getId();
    }

    public void update(Article article) {

        if (article.getId() == null) {
            throw new RuntimeException("Article id must be set");
        }

        articleMapper.update(article);
    }

    public void delete(Long id) {
        Article foundArticle = articleMapper.find(id);

        if (foundArticle == null) {
            throw new ArticleNotFoundException("Article not found");
        }

        articleMapper.delete(foundArticle);

    }

    public Article find(Long id) {
        Article article = articleMapper.find(id);
        articleMapper.findCommentsByArticleId(id);
        articleMapper.findTagsByArticleId(id);

        if (article == null) return null;

        return article;
    }

    public Collection<Article> findAll() {
        return articleMapper.findAll();
    }

    public void addComment(Long articleId, Comment comment) {
        Article foundArticle = articleMapper.find(articleId);
        comment.setArticle(foundArticle);
        commentMapper.save(comment);

    }

    @Override
    public Comment getComment(Long articleId, Long commentId) {
        Article article = articleMapper.find(articleId);

        if (article == null) throw new ArticleNotFoundException("There is no such article");

        for (Comment comment : article.getComments()) {

            if (comment.getId().equals(commentId)) {
                return comment;
            }

        }
        return null;
    }

    public Article addTags(Long articleId, Tag tag) {
        Article foundArticle = articleMapper.find(articleId);
        tagMapper.save(tag);
        tagMapper.saveArticleDep(foundArticle, tag);

        return foundArticle;
    }

    @Override
    public Tag getTag(Long articleId, Long tagId) {
        Article foundArticle = articleMapper.find(articleId);

        if (foundArticle == null) throw new ArticleNotFoundException("There is no such article");

        for (Tag tag : foundArticle.getTags()) {

            if (tag.getId().equals(tagId)) {
                return tag;
            }

        }

        return null;
    }

    public void removeTagsFromArticle(Long articleId, Tag tag) {
        Article foundArticle = articleMapper.find(articleId);
        articleMapper.deleteTagDep(foundArticle, tag);
    }

}
