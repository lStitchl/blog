package com.solvegen.blog.services.mapper;

import com.solvegen.blog.mappers.TagMapper;
import com.solvegen.blog.mappers.oracleMappers.TagOracleMapper;
import com.solvegen.blog.model.Tag;
import com.solvegen.blog.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TagServiceMapperImpl implements TagService {

    @Autowired(required = false)
    private TagMapper tagMapper;

    public Tag newTag() {
        return new Tag();
    }

    public Long save(Tag tag) {
        tagMapper.save(tag);

        return tag.getId();
    }

    public void update(Tag tag) {

        if (tag.getId() == null) {
            throw new RuntimeException("Tag id must be set");
        }

        tagMapper.update(tag);
    }

    public void delete(Long id) {
        Tag foundTag = tagMapper.find(id);

        if (foundTag == null) {
            throw new RuntimeException("Tag not found");
        }

        tagMapper.delete(foundTag);
    }

    public Tag find(Long id) {
        Tag tag = tagMapper.find(id);

        if (tag == null) return null;

        return tag;
    }
}
