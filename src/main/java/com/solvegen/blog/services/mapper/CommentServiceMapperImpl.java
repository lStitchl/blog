package com.solvegen.blog.services.mapper;

import com.solvegen.blog.exceptions.ArticleNotFoundException;
import com.solvegen.blog.mappers.CommentMapper;
import com.solvegen.blog.model.Article;
import com.solvegen.blog.model.Comment;
import com.solvegen.blog.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceMapperImpl implements CommentService {

    @Autowired(required = false)
    private CommentMapper commentMapper;

    public Comment newComment() {
        return new Comment();
    }

    public Long save(Comment comment) {
        commentMapper.save(comment);

        return comment.getId();
    }

    public Long update(Comment comment) {

        if (comment.getId() == null) {
            throw new ArticleNotFoundException("Comment id must be set");
        }

        commentMapper.update(comment);
        return comment.getId();
    }

    public void delete(Long id) {
        Comment foundComment = commentMapper.find(id);

        if (foundComment == null) {
            throw new ArticleNotFoundException("Comment not found");
        }

        Article existentArticle = foundComment.getArticle();
        existentArticle.getComments().remove(foundComment);
        commentMapper.delete(foundComment);
    }

    public Comment find(Long id) {
        Comment comment = commentMapper.find(id);

        if (comment == null) return null;

        return comment;
    }
}
