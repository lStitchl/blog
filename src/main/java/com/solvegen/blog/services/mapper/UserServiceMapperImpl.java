package com.solvegen.blog.services.mapper;

import com.solvegen.blog.mappers.UserMapper;
import com.solvegen.blog.mappers.h2mappers.UserH2Mapper;
import com.solvegen.blog.model.User;
import com.solvegen.blog.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserServiceMapperImpl implements UserService {

    @Autowired(required = false)
    private UserMapper userMapper;

    public User newUser() {
        return new User();
    }

    public Long save(User user) {

        userMapper.save(user);

        return user.getId();
    }

    public Long update(User user) {

        if (user.getId() == null) {
            throw new RuntimeException("User id must be set");
        }

        userMapper.update(user);
        return user.getId();
    }

    public void delete(Long id) {
        User foundUser = userMapper.find(id);

        if (foundUser == null) {
            throw new RuntimeException("User not found");
        }

        userMapper.delete(foundUser);
    }

    public User find(Long id) {
        User user = userMapper.find(id);

        if(user == null) return null;

        return user;
    }

}
