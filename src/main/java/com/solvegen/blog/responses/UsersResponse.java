package com.solvegen.blog.responses;

import com.solvegen.blog.model.User;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UsersResponse {

    private User user;

    private String message;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
