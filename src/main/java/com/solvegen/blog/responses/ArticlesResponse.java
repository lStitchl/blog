package com.solvegen.blog.responses;

import com.solvegen.blog.model.Article;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;


@XmlRootElement
public class ArticlesResponse {

    private Article article;

    private String message;

    private Collection<Article> articles;

    public Collection<Article> getArticles() {
        return articles;
    }

    public void setArticles(Collection<Article> articles) {
        this.articles = articles;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
