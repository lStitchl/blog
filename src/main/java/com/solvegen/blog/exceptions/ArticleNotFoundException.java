package com.solvegen.blog.exceptions;

import javax.xml.bind.annotation.XmlRootElement;

//@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="No such article")
@XmlRootElement
public class ArticleNotFoundException extends BlogException {

    private String exceptionMsg;

    public ArticleNotFoundException(String exceptionMsg) {

        this.exceptionMsg = exceptionMsg;

    }

    public String getExceptionMsg() {

        return exceptionMsg;

    }

    public void setExceptionMsg(String exceptionMsg) {

        this.exceptionMsg = exceptionMsg;

    }

}


