package com.solvegen.blog.controller;

import com.solvegen.blog.model.Tag;
import com.solvegen.blog.responses.TagsResponse;
import com.solvegen.blog.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class TagController {

    @Autowired
    TagService tagService;

    @RequestMapping(value = "/tags/{tagId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_XML_VALUE)
    public
    @ResponseBody
    void delete(@PathVariable Long tagId) {
        tagService.delete(tagId);
    }

    @RequestMapping(value = "/tags/{tagId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public
    @ResponseBody
    TagsResponse findTagById(@PathVariable Long tagId) {
        Tag foundTag = tagService.find(tagId);
        TagsResponse response = new TagsResponse();
        response.setTag(foundTag);

        return response;
    }
}
