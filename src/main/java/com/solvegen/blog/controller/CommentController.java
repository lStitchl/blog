package com.solvegen.blog.controller;

import com.solvegen.blog.model.Comment;
import com.solvegen.blog.responses.CommentsResponse;
import com.solvegen.blog.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class CommentController {

    @Autowired
    CommentService commentService;

    @RequestMapping(value = "/articles/{articleId}/comments/{commentId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_XML_VALUE)
    public
    @ResponseBody
    void deleteComment(@PathVariable Long commentId) {
        commentService.delete(commentId);
    }

    @RequestMapping(value = "/comments/{commentId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public
    @ResponseBody
    CommentsResponse findCommentById(@PathVariable Long commentId) {
        Comment foundComment = commentService.find(commentId);
        CommentsResponse response = new CommentsResponse();
        response.setComment(foundComment);

        return response;
    }

}
