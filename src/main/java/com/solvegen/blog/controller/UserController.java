package com.solvegen.blog.controller;

import com.solvegen.blog.model.User;
import com.solvegen.blog.responses.UsersResponse;
import com.solvegen.blog.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController()
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public
    @ResponseBody
    UsersResponse findUser(@PathVariable Long id) {
        User user = userService.find(id);
        UsersResponse response = new UsersResponse();
        response.setUser(user);

        return response;
    }

    @RequestMapping(value = "/user/newuser", method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseBody
    UsersResponse insertUser(@RequestBody User user) {
        Long userId = userService.save(user);
        User savedUser = userService.find(userId);
        UsersResponse response = new UsersResponse();
        response.setUser(savedUser);

        return response;
    }

    @RequestMapping(value = "/user/{id}/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE)
    public
    @ResponseBody
    UsersResponse updateUser(@RequestBody User user) {
        UsersResponse response = new UsersResponse();
        userService.update(user);
        response.setUser(user);

        return response;
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_XML_VALUE)
    public
    @ResponseBody
    void deleteUser(@PathVariable Long id) {
        userService.delete(id);
    }
}
