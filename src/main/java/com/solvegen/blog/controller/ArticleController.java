package com.solvegen.blog.controller;

import com.solvegen.blog.exceptions.ArticleNotFoundException;
import com.solvegen.blog.model.Article;
import com.solvegen.blog.model.Comment;
import com.solvegen.blog.model.Tag;
import com.solvegen.blog.responses.ArticlesResponse;
import com.solvegen.blog.responses.CommentsResponse;
import com.solvegen.blog.responses.ExceptionsResponse;
import com.solvegen.blog.responses.TagsResponse;
import com.solvegen.blog.services.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController()
public class ArticleController {

    @Autowired
    ArticleService articleService;

    @RequestMapping(value = "/articles", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public ArticlesResponse findAllArticles() {
        ArticlesResponse response = new ArticlesResponse();
        response.setArticles(articleService.findAll());

        return response;
    }

    @RequestMapping(value = "/articles/newarticle", method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseBody
    ArticlesResponse insertArticle(@RequestBody Article article) {
        Long articleId = articleService.save(article);
        Article savedArticle = articleService.find(articleId);
        ArticlesResponse response = new ArticlesResponse();
        response.setArticle(savedArticle);

        return response;
    }

    @RequestMapping(value = "/articles/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public
    @ResponseBody
    ArticlesResponse findArticle(@PathVariable Long id) {
        Article article = articleService.find(id);
        ArticlesResponse response = new ArticlesResponse();
        response.setArticle(article);

        return response;
    }

    @RequestMapping(value = "/articles/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_XML_VALUE)
    public
    @ResponseBody
    ArticlesResponse deleteArticle(@PathVariable Long id) throws ArticleNotFoundException {
        articleService.delete(id);
        ArticlesResponse response = new ArticlesResponse();
        response.setMessage("Article deleted successfully!");
        return response;
    }

    @ExceptionHandler(ArticleNotFoundException.class)
    public
    @ResponseBody
    ExceptionsResponse exceptionResponse() {
        ExceptionsResponse response = new ExceptionsResponse();
        response.setMessage("There is no such article");

        return response;
    }

    @RequestMapping(value = "/articles/{id}/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE)
    public
    @ResponseBody
    ArticlesResponse updateArticle(@RequestBody Article article) {
        ArticlesResponse response = new ArticlesResponse();
        articleService.update(article);
        response.setArticle(article);

        return response;
    }

    @RequestMapping(value = "/articles/{id}/comments", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public
    @ResponseBody
    CommentsResponse getComments(@PathVariable Long id) {
        Article article = articleService.find(id);
        CommentsResponse response = new CommentsResponse();

        if (article == null) return response;

        response.setComments(article.getComments());

        return response;
    }

    @RequestMapping(value = "/articles/{articleId}/comments/newcomment", method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE)
    public
    @ResponseBody
    void addComment(@PathVariable Long articleId, @RequestBody Comment comment) {
        articleService.addComment(articleId, comment);
    }

    @RequestMapping(value = "/articles/{articleId}/comments/{commentId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public
    @ResponseBody
    CommentsResponse getComment(@PathVariable Long articleId, @PathVariable Long commentId) {
        Comment foundComment = articleService.getComment(articleId, commentId);
        CommentsResponse response = new CommentsResponse();
        response.setComment(foundComment);

        return response;
    }

    @RequestMapping(value = "/articles/{id}/tags", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public
    @ResponseBody
    TagsResponse getTags(@PathVariable Long id) {
        Article article = articleService.find(id);
        TagsResponse response = new TagsResponse();

        if (article == null) return response;

        response.setTags(article.getTags());

        return response;
    }

    @RequestMapping(value = "/articles/{articleId}/tags/newtag", method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE)
    public
    @ResponseBody
    void addTag(@PathVariable Long articleId, @RequestBody Tag tag) {
        articleService.addTags(articleId, tag);
    }

    @RequestMapping(value = "/articles/{articleId}/tags/{tagId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public
    @ResponseBody
    TagsResponse getTag(@PathVariable Long articleId, @PathVariable Long tagId) {
        Tag foundTag = articleService.getTag(articleId, tagId);
        TagsResponse response = new TagsResponse();
        response.setTag(foundTag);

        return response;
    }

    @RequestMapping(value = "/articles/{articleId}/tags/{tagId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE)
    public
    @ResponseBody
    void removeTagFromArticle(@PathVariable Long articleId, @RequestBody Tag tag) {
        articleService.removeTagsFromArticle(articleId, tag);
    }

}