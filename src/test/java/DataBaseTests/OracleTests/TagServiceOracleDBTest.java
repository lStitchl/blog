package DataBaseTests.OracleTests;

import com.solvegen.blog.model.Tag;
import com.solvegen.blog.services.TagService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/spring/spring-config.xml")
@ActiveProfiles("mybatis")
public class TagServiceOracleDBTest {
    private static final Long EXISTENT_TAG_ID = 2L;

    @Autowired
    TagService tagService;

    @Test
    public void test1_Find() {
        Tag foundTag = tagService.find(EXISTENT_TAG_ID);
        String content = "content";

        assertEquals(EXISTENT_TAG_ID, foundTag.getId());
        assertEquals(content, foundTag.getContent());
    }

    @Test
    public void test2_Insert() {
        Tag tag = new Tag();
        String content = "content";
        tag.setContent(content);
        Long tagId = tagService.save(tag);
        Tag insertedTag = tagService.find(tagId);

        assertNotNull(tagId);
        Assert.assertEquals(content, insertedTag.getContent());
    }

    @Test
    public void test3_Update() {
        Tag foundTag = tagService.find(1L);
        String oldContent = foundTag.getContent();

        String newContent = "new content";
        foundTag.setContent(newContent);

        tagService.update(foundTag);
        Tag updatedTag = tagService.find(foundTag.getId());
        String updatedContent = updatedTag.getContent();

        assertNotEquals(oldContent, updatedContent);
        assertEquals(newContent, foundTag.getContent());
    }

    @Test
    public void test4_Delete() {
        Tag foundTag = tagService.find(EXISTENT_TAG_ID);

        assertEquals(EXISTENT_TAG_ID, foundTag.getId());

        tagService.delete(foundTag.getId());

        assertNull(tagService.find(EXISTENT_TAG_ID));
    }

}
