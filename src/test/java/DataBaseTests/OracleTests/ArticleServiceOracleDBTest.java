package DataBaseTests.OracleTests;

import com.solvegen.blog.model.Article;
import com.solvegen.blog.model.Comment;
import com.solvegen.blog.model.Tag;
import com.solvegen.blog.model.User;
import com.solvegen.blog.services.ArticleService;
import com.solvegen.blog.services.CommentService;
import com.solvegen.blog.services.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/spring/spring-config.xml")
@ActiveProfiles("mybatis")


public class ArticleServiceOracleDBTest {

    private static final Long EXISTENT_ARTICLE_ID = 23L;

    private static final Long EXISTENT_COMMENT_ID = 1L;

    private static final Long EXISTENT_USER_ID = 2L;

    @Autowired(required = false)
    ArticleService articleService;

    @Autowired(required = false)
    UserService userService;

    @Autowired(required = false)
    CommentService commentService;

    @Test
    public void test1_Find() {
        Article foundArticle = articleService.find(EXISTENT_ARTICLE_ID);
        Long foundArticleId = foundArticle.getId();
        Long authorID = 2L;
        String title = "title";
        String content = "content";

        assertEquals(EXISTENT_ARTICLE_ID, foundArticleId);
        assertEquals(authorID, foundArticle.getAuthor().getId());
        assertEquals(content, foundArticle.getContent());
        assertEquals(title, foundArticle.getTitle());
    }

    @Test
    public void test2_Insert() {
        Article article = new Article();
        String title = "title";
        String content = "content";
        article.setTitle(title);
        article.setContent(content);
        //Timestamp createdAt = new Timestamp(new Date().getTime());
        //article.setCreatedAt(createdAt);
        article.setAuthor(userService.find(EXISTENT_USER_ID));
        Long articleId = articleService.save(article);
        Article savedArticle = articleService.find(articleId);

        assertNotNull(articleId);
        assertEquals(title, savedArticle.getTitle());
        assertEquals(content, savedArticle.getContent());
        //assertEquals(EXISTENT_USER_ID, savedArticle.getAuthor().getId());
    }

    @Test
    public void test3_FindAll() {
        Collection<Article> allArticles = articleService.findAll();
        Article existentArticle = allArticles.iterator().next();
        Long articleId = 23L;
        Long authorId = 2L;
        String title = "title";
        String content = "content";

        assertEquals(4, allArticles.size());
        assertEquals(articleId, existentArticle.getId());
        assertEquals(authorId, existentArticle.getAuthor().getId());
        assertEquals(content, existentArticle.getContent());
        assertEquals(title, existentArticle.getTitle());
    }

    @Test
    public void test4_GetTags() {
        Article createdArticle = articleService.find(EXISTENT_ARTICLE_ID);

        assertTrue(createdArticle.getTags().size() > 0);

        Long existentTagId = 23L;
        String existentTagContent = "content";
        Tag foundTag = createdArticle.getTags().iterator().next();

        assertEquals(existentTagId, foundTag.getId());
        assertEquals(existentTagContent, foundTag.getContent());
    }

    @Test
    public void test5_GetComments() {
        Article createdArticle = articleService.find(EXISTENT_ARTICLE_ID);

        assertTrue(createdArticle.getComments().size() > 0);

        Long existentCommentId = 1L;
        String existentCommentContent = "111";
        Comment foundComment = createdArticle.getComments().iterator().next();

        assertEquals(existentCommentId, foundComment.getId());
        assertEquals(existentCommentContent, foundComment.getContent());
    }

    @Test
    public void test6_Update() {
        Article foundArticle = articleService.find(EXISTENT_ARTICLE_ID);
        String oldContent = foundArticle.getContent();
        String oldTitle = foundArticle.getTitle();

        String newContent = "new content";
        String newTitle = "title title title";
        foundArticle.setContent(newContent);
        foundArticle.setTitle(newTitle);

        articleService.update(foundArticle);
        Article updatedArticle = articleService.find(EXISTENT_ARTICLE_ID);
        String updatedContent = updatedArticle.getContent();
        String updatedTitle = updatedArticle.getTitle();

        assertNotEquals(oldContent, updatedContent);
        assertNotEquals(oldTitle, updatedTitle);

        assertEquals(newContent, updatedContent);
        assertEquals(newTitle, updatedTitle);
    }

    @Test
    public void test7_AddComments() {
        Article foundArticle = articleService.find(EXISTENT_ARTICLE_ID);

        assertEquals(foundArticle.getId(), EXISTENT_ARTICLE_ID);

        Comment comment = new Comment();
        String content = "title";
        User user = userService.find(EXISTENT_USER_ID);
        Article article = articleService.find(EXISTENT_ARTICLE_ID);
        Timestamp createdAt = new Timestamp(new Date().getTime());

        comment.setContent(content);
        comment.setAuthor(user);
        comment.setCreatedAt(createdAt);
        comment.setArticle(article);
        foundArticle.addComment(comment);

        assertTrue(foundArticle.getComments().contains(comment));
    }

    @Test
    public void test8_Delete() {
        Article foundArticle = articleService.find(24L);
        Long id = 24L;

        assertEquals(foundArticle.getId(), id);
        Assert.assertEquals((Long) 45L, foundArticle.getTags().iterator().next().getId());
        //assertEquals(1, foundArticle.getComments().size());
        //assertEquals((Long) 3L, foundArticle.getComments().iterator().next().getId());
        try {
            articleService.delete(foundArticle.getId());
        } catch (Exception e) {

        }
        assertNull(articleService.find(24L));
        //assertNull(commentService.find(3L));
    }

}
