package DataBaseTests.H2Tests;

import com.solvegen.blog.model.User;
import com.solvegen.blog.services.CommentService;
import com.solvegen.blog.services.UserService;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/spring/spring-config.xml")
@ActiveProfiles("mybatis")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class UserServiceH2DBTest {
    private static final Long EXISTENT_USER_ID = 1L;

    @Autowired(required = false)
    UserService userService;

    @Autowired
    CommentService commentService;

    @Test
    public void test1_Find() {
        User foundUser = userService.find(EXISTENT_USER_ID);
        String name = "11";
        String sirname = "111";
        String nickname = "1111";
        String password = "11111";
        String email = "11";

        assertEquals(EXISTENT_USER_ID, foundUser.getId());
        assertEquals(name, foundUser.getName());
        assertEquals(sirname, foundUser.getSirname());
        assertEquals(nickname, foundUser.getNickname());
        assertEquals(password, foundUser.getPassword());
        assertEquals(email, foundUser.getMail());
    }

    @Test
    public void test2_Insert() {
        User user = new User();
        String name = "Bob";
        String sirname = "Bobbins";
        String nickname = "John Smith";
        String password = "1234567";
        Date dateOfBirth = new Date();
        String email = "bobmail@mail.com";

        user.setName(name);
        user.setSirname(sirname);
        user.setNickname(nickname);
        user.setPassword(password);
        user.setDateOfBirth(dateOfBirth);
        user.setMail(email);
        Long userId = userService.save(user);
        User foundUser = userService.find(userId);

        assertNotNull(userId);                                          //!!!!!!!
        assertEquals(name, foundUser.getName());
        assertEquals(sirname, foundUser.getSirname());
        assertEquals(nickname, foundUser.getNickname());
        assertEquals(password, foundUser.getPassword());
        assertEquals(dateOfBirth.after(dateOfBirth), foundUser.getDateOfBirth().after(foundUser.getDateOfBirth()));
        assertEquals(email, foundUser.getMail());
    }

    @Test
    public void test3_Update() {
        User foundUser = userService.find(EXISTENT_USER_ID);
        foundUser.setName("New name");
        foundUser.setSirname("NewBobbins");
        foundUser.setNickname("NewJohn Smith");
        foundUser.setPassword("Newpassword");
        foundUser.setMail("newmymail@mymail.com");
        userService.update(foundUser);
        User updatedUser = userService.find(EXISTENT_USER_ID);

        assertEquals("New name", updatedUser.getName());
        assertEquals("NewBobbins", updatedUser.getSirname());
        assertEquals("NewJohn Smith", updatedUser.getNickname());
        assertEquals("New name", updatedUser.getName());
        assertEquals("newmymail@mymail.com", updatedUser.getMail());
    }

    @Test
    public void test4_Delete() {
        User foundUser = userService.find(EXISTENT_USER_ID);

        assertEquals(foundUser.getId(), EXISTENT_USER_ID);

        userService.delete(EXISTENT_USER_ID);

        assertNull(userService.find(EXISTENT_USER_ID));
    }

}
