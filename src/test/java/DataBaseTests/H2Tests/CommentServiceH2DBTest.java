package DataBaseTests.H2Tests;

import com.solvegen.blog.model.Article;
import com.solvegen.blog.model.Comment;
import com.solvegen.blog.model.User;
import com.solvegen.blog.services.ArticleService;
import com.solvegen.blog.services.CommentService;
import com.solvegen.blog.services.UserService;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;
import java.util.Date;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/spring/spring-config.xml")
@ActiveProfiles("mybatis")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class CommentServiceH2DBTest {
    private static final Long EXISTENT_ARTICLE_ID = 2L;

    private static final Long EXISTENT_COMMENT_ID = 2L;

    private static final Long EXISTENT_USER_ID = 2L;

    @Autowired(required = false)
    CommentService commentService;

    @Autowired(required = false)
    ArticleService articleService;

    @Autowired(required = false)
    UserService userService;

    @Test
    public void test1_Find() {
        Comment foundComment = commentService.find(EXISTENT_COMMENT_ID);
        String foundCommentContent = "222";

        assertEquals(EXISTENT_COMMENT_ID, foundComment.getId());
        assertEquals(foundCommentContent, foundComment.getContent());
        assertEquals(EXISTENT_USER_ID, foundComment.getAuthor().getId());
        assertEquals(EXISTENT_ARTICLE_ID, foundComment.getArticle().getId());
    }

    @Test
    public void test2_Insert() {
        Comment comment = new Comment();
        String content = "content";
        User user = userService.find(EXISTENT_USER_ID);
        Article article = articleService.find(EXISTENT_ARTICLE_ID);
        Timestamp createdAt = new Timestamp(new Date().getTime());

        comment.setContent(content);
        comment.setAuthor(user);
        comment.setCreatedAt(createdAt);
        comment.setArticle(article);

        Long commentId = commentService.save(comment);
        Comment insertedComment = commentService.find(commentId);

        assertNotNull(commentId);
        assertEquals(content, insertedComment.getContent());
        assertEquals(EXISTENT_USER_ID, insertedComment.getAuthor().getId());
        assertEquals(createdAt, insertedComment.getCreatedAt());
    }

    @Test
    public void test3_Update() {
        Comment foundComment = commentService.find(EXISTENT_COMMENT_ID);
        String oldContent = foundComment.getContent();
        User oldModifier = foundComment.getModifiedBy();

        User newModifier = userService.find(EXISTENT_USER_ID);
        String newContent = "new new content";
        foundComment.setModifiedBy(newModifier);
        foundComment.setContent(newContent);

        commentService.update(foundComment);
        Comment updatedComment = commentService.find(foundComment.getId());
        User updatedModifier = updatedComment.getModifiedBy();
        String updatedContent = updatedComment.getContent();

        assertNotEquals(oldContent, updatedContent);
        assertNotEquals(oldModifier, updatedModifier);

        assertEquals(newModifier.getId(), updatedModifier.getId());
        assertEquals(newContent, updatedContent);
    }

    @Test
    public void test4_Delete() {

        Comment foundComment = commentService.find(EXISTENT_COMMENT_ID);

        assertEquals(foundComment.getId(), EXISTENT_COMMENT_ID);

        commentService.delete(foundComment.getId());

        assertNull(commentService.find(EXISTENT_COMMENT_ID));
    }
}
