package ServiceTests;

import com.solvegen.blog.mappers.UserMapper;
import com.solvegen.blog.model.User;
import com.solvegen.blog.services.mapper.UserServiceMapperImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    private static final Long EXISTENT_USER_ID = 1L;

    private static final Long NON_EXISTENT_USER_ID = 5L;

    @Mock
    private User user;

    @Mock
    private UserMapper userMapper;

    @InjectMocks
    private UserServiceMapperImpl userService;

    @Before
    public void setUp() {
        when(user.getId()).thenReturn(EXISTENT_USER_ID);
    }

    @Test
    public void newUser() {
        Assert.assertNotNull(userService.newUser());
    }

    @Test
    public void save() {

        when(userMapper.save(user)).thenReturn(EXISTENT_USER_ID);

        Long userId = userService.save(user);

        Assert.assertEquals(userId, EXISTENT_USER_ID);
    }

    @Test(expected = RuntimeException.class)
    public void update() {
        when(user.getId()).thenReturn(null);
        userService.update(user);

        verify(userMapper).update(user);
    }

    @Test
    public void deleteExistentUser() {

        when(userMapper.find(EXISTENT_USER_ID)).thenReturn(user);

        userService.delete(user.getId());

        verify(userMapper).delete(user);
    }

    @Test(expected = RuntimeException.class)
    public void notSuccessfulDelete() {

        User user = mock(User.class);
        when(user.getId()).thenReturn(NON_EXISTENT_USER_ID);

        userService.delete(user.getId());

        verify(userMapper, times(0)).delete(any(User.class));

    }

    @Test
    public void find() {

        when(userMapper.find(EXISTENT_USER_ID)).thenReturn(user);

        User foundUser = userService.find(EXISTENT_USER_ID);

        verify(userMapper, times(1)).find(EXISTENT_USER_ID);
        Assert.assertEquals(user, foundUser);
    }

}
