package ServiceTests;

import com.solvegen.blog.mappers.TagMapper;
import com.solvegen.blog.model.Tag;
import com.solvegen.blog.services.mapper.TagServiceMapperImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {

    private static final Long EXISTENT_TAG_ID = 1L;

    private static final Long NON_EXISTENT_TAG_ID = 5L;

    @Mock
    TagMapper tagMapper;

    @Mock
    private Tag tag;

    @InjectMocks
    private TagServiceMapperImpl tagService;

    @Before
    public void setUp() {
        when(tag.getId()).thenReturn(EXISTENT_TAG_ID);
    }

    @Test
    public void newTag() {
        Assert.assertNotNull(tagService.newTag());
    }

    @Test
    public void save() {
        when(tagMapper.save(tag)).thenReturn(EXISTENT_TAG_ID);
        when(tag.getId()).thenReturn(EXISTENT_TAG_ID);

        Long tagId = tagService.save(tag);

        Assert.assertEquals(tagId, EXISTENT_TAG_ID);
    }

    @Test(expected = RuntimeException.class)
    public void update() {
        when(tag.getId()).thenReturn(null);

        tagService.update(tag);

        verify(tagMapper).update(tag);
    }

    @Test
    public void deleteExistentTag() {

        when(tagMapper.find(EXISTENT_TAG_ID)).thenReturn(tag);

        tagService.delete(tag.getId());

        verify(tagMapper).delete(tag);
    }

    @Test(expected = RuntimeException.class)
    public void notSuccessfulDelete() {

        Tag tag = mock(Tag.class);
        when(tag.getId()).thenReturn(NON_EXISTENT_TAG_ID);

        tagService.delete(tag.getId());

        verify(tagMapper, times(0)).delete(any(Tag.class));
    }

    @Test
    public void find() {

        when(tagMapper.find(EXISTENT_TAG_ID)).thenReturn(tag);

        Tag foundUser = tagService.find(EXISTENT_TAG_ID);

        verify(tagMapper, times(1)).find(EXISTENT_TAG_ID);
        Assert.assertEquals(tag, foundUser);
    }

}
