package ServiceTests;

import com.solvegen.blog.exceptions.ArticleNotFoundException;
import com.solvegen.blog.mappers.ArticleMapper;
import com.solvegen.blog.mappers.CommentMapper;
import com.solvegen.blog.mappers.TagMapper;
import com.solvegen.blog.model.Article;
import com.solvegen.blog.model.Comment;
import com.solvegen.blog.model.Tag;
import com.solvegen.blog.services.mapper.ArticleServiceMapperImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ArticleServiceTest {

    private static final Long EXISTENT_ARTICLE_ID = 1L;

    private static final Long NON_EXISTENT_ARTICLE_ID = 5L;

    private static final Long EXISTENT_TAG_ID = 1L;

    private static final Long EXISTENT_COMMENT_ID = 1L;

    private Collection<Article> articleList;

    @Mock
    private ArticleMapper articleMapper;

    @Mock
    private CommentMapper commentMapper;

    @Mock
    private TagMapper tagMapper;

    @Mock
    private Article article;

    @Mock
    private Article secondArticle;

    @Spy
    private Comment comment;

    @Spy
    private Tag tag;

    @InjectMocks
    private ArticleServiceMapperImpl articleService;

    @Before
    public void setUp() {
        articleList = Arrays.asList(article, secondArticle);
        when(article.getId()).thenReturn(EXISTENT_ARTICLE_ID);
        when(articleMapper.find(EXISTENT_ARTICLE_ID)).thenReturn(article);
    }

    @Test

    public void newArticle() {
        Assert.assertNotNull(articleService.newArticle());
    }

    @Test
    public void save() {
        when(articleMapper.save(article)).thenReturn(EXISTENT_ARTICLE_ID);

        Long articleId = articleService.save(article);

        Assert.assertEquals(articleId, EXISTENT_ARTICLE_ID);

    }

    @Test(expected = RuntimeException.class)
    public void update() {
        when(article.getId()).thenReturn(null);

        articleService.update(article);

        verify(articleMapper).update(article);
    }

    @Test
    public void deleteExistentArticle() {
        when(articleMapper.find(EXISTENT_ARTICLE_ID)).thenReturn(article);

        try {
            articleService.delete(EXISTENT_ARTICLE_ID);
        } catch (Exception e) {

        }
        verify(articleMapper).delete(article);
    }

    @Test(expected = ArticleNotFoundException.class)
    public void notSuccessfulDelete() {
        when(articleMapper.find(NON_EXISTENT_ARTICLE_ID)).thenReturn(null);

        articleService.delete(NON_EXISTENT_ARTICLE_ID);

        verify(articleMapper, times(0)).delete(any(Article.class));
    }

    @Test
    public void find() {
        when(articleMapper.find(EXISTENT_ARTICLE_ID)).thenReturn(article);

        Article foundArticle = articleService.find(EXISTENT_ARTICLE_ID);

        verify(articleMapper, times(1)).find(EXISTENT_ARTICLE_ID);
        Assert.assertEquals(article, foundArticle);
    }

    @Test
    public void findAll() {
        when(articleMapper.findAll()).thenReturn(articleList);

        Collection<Article> articles = articleMapper.findAll();

        verify(articleMapper, times(1)).findAll();
        assertTrue(articles.contains(article));
        assertTrue(articles.contains(secondArticle));
        Assert.assertTrue(articles.size() == articleList.size());
    }

    @Test
    public void addComment() {
        when(articleMapper.find(EXISTENT_ARTICLE_ID)).thenReturn(article);
        when(comment.getId()).thenReturn(EXISTENT_COMMENT_ID);
        when(commentMapper.save(comment)).thenReturn(EXISTENT_COMMENT_ID);

        articleService.addComment(EXISTENT_ARTICLE_ID, comment);

        InOrder order = inOrder(articleMapper, comment, commentMapper);

        order.verify(articleMapper, times(1)).find(EXISTENT_ARTICLE_ID);
        order.verify(comment, times(1)).setArticle(eq(article));
        order.verify(commentMapper, times(1)).save(eq(comment));
    }

    @Test
    public void addTags() {
        when(articleMapper.find(EXISTENT_ARTICLE_ID)).thenReturn(article);
        when(tag.getId()).thenReturn(EXISTENT_TAG_ID);
        when(tagMapper.save(tag)).thenReturn(EXISTENT_TAG_ID);

        articleService.addTags(EXISTENT_ARTICLE_ID, tag);

        InOrder order = inOrder(articleMapper, tagMapper, tagMapper);

        order.verify(articleMapper, times(1)).find(EXISTENT_ARTICLE_ID);
        order.verify(tagMapper, times(1)).save(eq(tag));
        order.verify(tagMapper, times(1)).saveArticleDep(eq(article), eq(tag));
    }

}

