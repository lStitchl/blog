package ServiceTests;

import com.solvegen.blog.mappers.CommentMapper;
import com.solvegen.blog.model.Article;
import com.solvegen.blog.model.Comment;
import com.solvegen.blog.services.mapper.CommentServiceMapperImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {

    private static final Long EXISTENT_COMMENT_ID = 2L;

    private static final Long NON_EXISTENT_COMMENT_ID = 5L;

    @Mock
    CommentMapper commentMapper;

    @Mock
    private Comment comment;

    @Mock
    private Article article;

    @InjectMocks
    private CommentServiceMapperImpl commentService;

    @Before
    public void setUp() {
        when(comment.getId()).thenReturn(EXISTENT_COMMENT_ID);
    }

    @Test
    public void newComment() {
        Assert.assertNotNull(commentService.newComment());
    }

    @Test
    public void save() {
        when(commentMapper.save(comment)).thenReturn(EXISTENT_COMMENT_ID);
        when(comment.getArticle()).thenReturn(new Article());
        Long commentId = commentService.save(comment);

        Assert.assertEquals(commentId, EXISTENT_COMMENT_ID);
    }

    @Test(expected = RuntimeException.class)
    public void update() {
        when(comment.getId()).thenReturn(null);

        commentService.update(comment);

        verify(commentMapper).update(comment);
    }

    @Test
    public void deleteExistentComment() {

        when(commentMapper.find(EXISTENT_COMMENT_ID)).thenReturn(comment);
        when(comment.getId()).thenReturn(EXISTENT_COMMENT_ID);
        when(comment.getArticle()).thenReturn(article);
        when(article.getComments()).thenReturn(new ArrayList<Comment>());

        commentService.delete(comment.getId());

        verify(commentMapper).delete(comment);
    }

    @Test(expected = RuntimeException.class)
    public void notSuccessfulDelete() {

        when(commentMapper.find(NON_EXISTENT_COMMENT_ID)).thenReturn(null);

        commentService.delete(comment.getId());

        verify(commentMapper, times(0)).delete(any(Comment.class));
    }

    @Test
    public void find() {

        when(commentMapper.find(EXISTENT_COMMENT_ID)).thenReturn(comment);

        Comment foundComment = commentService.find(EXISTENT_COMMENT_ID);

        verify(commentMapper, times(1)).find(EXISTENT_COMMENT_ID);
        Assert.assertEquals(comment, foundComment);
    }

}

